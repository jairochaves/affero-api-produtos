import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Produto from 'App/Models/Produto'
import StoreProdutoValidator from 'App/Validators/StoreProdutoValidator'
import UpdateProdutoValidator from 'App/Validators/UpdateProdutoValidator'

export default class ProdutosController {
  public async index({}: HttpContextContract) {
    return (await Produto.all()).reverse()
  }

  public async store({ request }: HttpContextContract) {
    const data = await request.validate(StoreProdutoValidator)
    return await Produto.create(data)
  }

  public async show({ params }: HttpContextContract) {
    return await Produto.findOrFail(params.id)
  }

  public async update({ request, params }: HttpContextContract) {
    const data = await request.validate(UpdateProdutoValidator)
    const produto = await Produto.findOrFail(params.id)
    produto.merge(data)
    await produto.save()
  }

  public async destroy({ params }: HttpContextContract) {
    const produto = await Produto.findOrFail(params.id)
    await produto.delete()
  }
}
