import { schema, rules } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class StoreProdutoValidator {
  constructor(protected ctx: HttpContextContract) {}

  public schema = schema.create({
    nome: schema.string({ trim: true }),
    codigoBarras: schema.string({ trim: true }),
    descricao: schema.string({ trim: true }),
    quantidade: schema.number([rules.range(1, 100000)]),
    categoria: schema.string({ trim: true }),
  })

  public messages = {}
}
