import { schema } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class UpdateProdutoValidator {
  constructor(protected ctx: HttpContextContract) {}

  public schema = schema.create({
    nome: schema.string.optional({ trim: true }),
    codigoBarras: schema.string.optional({ trim: true }),
    descricao: schema.string.optional({ trim: true }),
    quantidade: schema.number.optional(),
    categoria: schema.string.optional({ trim: true }),
  })

  public messages = {}
}
