## Para que o projeto fucione, você precisa definir as variáveis de ambiente. Para isso:
- Copie e renomei o arquivo `.env.example` para `.env`
- Defina os valores como julgar necessário.
- Caso prefira, apenas insira os dados do seu banco de dados nas seguinte variáveis.
    - `MYSQL_HOST` - O endereço do host mysql
    - `MYSQL_PORT` - A porta do seu servidor mysql  
    - `MYSQL_USER` - O nome de usuário mysql
    - `MYSQL_PASSWORD` - A senha correspondente ao usuário acima.
    - `MYSQL_DB_NAME` - O nome do banco de dados.
- Caso prerifa utilizar outro banco de dados, consulte a [documentação do Adonisjs](https://docs.adonisjs.com/guides/database/introduction)

## Para instalar as dependências, execute:

### `yarn` ou `npm install`

## Para que as migrações do banco de dados sejam executadas, e crie as tabelas do banco de dados, execute:

### `node ace migration:run`

## Para iniciar o projeto em modo de desenvolvimento, execute:

### `yarn dev` ou `npm run dev`
